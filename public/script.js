function chercheImage(motRecherche) {
    return fetch("https://api.pexels.com/v1/search?query=" + motRecherche, {
        headers: { Authorization: "iUIuBzFoQmskG8hisaQdGL25oTKx0PKkENur6eheaUYUgVpBGWVnOJ2S" }
    })
        .then(response => {
            return response.json(); //retourne la chaine de donnée
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error('Error:', error);
            throw error; 
        });
}

document.addEventListener('DOMContentLoaded', function () {
  //definition des var
    var mainElement = document.getElementById('poster');
    var cadreElementab = [];
    var selectedElement = null;

    //creation des div
    for (var i = 1; i <= 4; i++) {
        var cadreElement = document.createElement('div');
        cadreElement.className = 'depot';
        cadreElement.textContent = 'image ' + i;
        mainElement.appendChild(cadreElement);
        cadreElementab.push(cadreElement);

        //detection du clique sur un carre
        cadreElement.addEventListener('click', function (event) {
        if (event.target.classList.contains('depot')) {
            cadreElementab.forEach(function (element) {
                if (element !== event.target) {
                    element.classList.remove('selected');
                }
            });
            event.target.classList.toggle('selected');
            selectedElement = event.target;
        }
        });
    }

    var asidelement = document.querySelector('aside');
    var imageElementab = [];

    
    //creation des images
    chercheImage("turtle")
        .then(listeImage => {
            for (var j = 1; j <= 6; j++) {
                var imageElement = document.createElement('img');
                
                imageElement.src = listeImage.photos[j].src.small ;
                imageElement.alt = 'imageapi' + j;
        
                imageElement.style.width = '100px';
                imageElement.style.height = '100px';
        
                asidelement.appendChild(imageElement);
                imageElementab.push(imageElement);
        
                //deplacement des images
                imageElement.addEventListener('click', function (event) {
                    if (selectedElement) {
                        var clone = event.target.cloneNode();
                        selectedElement.innerHTML = '';
                        selectedElement.appendChild(clone);
                    }
                });
            }
            
        
        })
        .catch(error => {
            console.error('Error in main:', error);
        });

        mainElement.addEventListener('click', function (event) {
            if (event.target.tagName === 'IMG') {
                // Remonter jusqu'à la div parente et la sélectionner
                var selectedDiv = event.target.closest('.depot');
                if (selectedDiv) {
                    cadreElementab.forEach(function (element) {
                        if (element !== selectedDiv) {
                            element.classList.remove('selected');
                        }
                    });
                    selectedDiv.classList.toggle('selected');
                    selectedElement = selectedDiv;
                }
            }
        });
    
  //pour de-selctionner si on clique hors du cadre
    document.addEventListener('click', function (event) {
        if (selectedElement && !selectedElement.contains(event.target)) {
            selectedElement.classList.remove('selected');
            selectedElement = null;
        }
    });

    //couleur
    var formElement = document.querySelector('form');
    var inputElement = document.getElementById('color');

    formElement.addEventListener('submit', function (event) {
        event.preventDefault(); // Pour éviter le rechargement de la page

        var inputValue = inputElement.value.toLowerCase();

        mainElement.style.backgroundColor = inputValue;
        
    });
});


